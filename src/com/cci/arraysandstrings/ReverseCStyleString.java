package com.cci.arraysandstrings;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Single;

public class ReverseCStyleString {

	public static void main(String[] args) {
		System.out.println(reverseCStyleString("Hello"));
		System.out.println(reverseCStyleString("Just hope "));
		System.out.println(reverseCStyleString("How are you?"));
	}
	
	private static String reverseCStyleString(String string) {
		
		char[] cArray = string.trim().toCharArray();
		int index = 0;
		int revIndex = string.trim().length()-1;
		char temp;
		
		while (revIndex > index) {
			temp = cArray[index];
			cArray[index] = cArray[revIndex];
			cArray[revIndex] = temp;
			index++;
			revIndex--;
		}
		
		return String.valueOf(cArray);
	}

}
