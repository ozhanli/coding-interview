package com.cci.arraysandstrings;

public class UniqueCharacters {

	public static void main(String[] args) {
		System.out.println(isStringCharactersUnique("Unique"));
		System.out.println(isStringCharactersUnique("NotUniquee"));
		System.out.println(isStringCharactersUnique("Hello"));
	}
	
	private static String isStringCharactersUnique(String string) {
		
		char[] cArray = string.toCharArray();
		
		for(int i=0; i<cArray.length; i++) {
			for(int j=i+1; j<cArray.length; j++) {
				if(Character.toLowerCase(cArray[i]) == Character.toLowerCase(cArray[j])) {
					return string + ": does not have all unique characters";
				}
			}
		}
		
		return string + ": has all unique characters";
	}

}
