package com.cci.arraysandstrings;

public class RemoveDuplicateChar {

	public static void main(String[] args) {
        removeDuplicateChar("aaabbb");
        removeDuplicateChar("Hello");
		removeDuplicateChar("Just hope ");
		removeDuplicateChar("How are you?");
	}
	
	private static void removeDuplicateChar(String string) {

        char[] cArray = string.toCharArray();

		if(cArray == null) return;
		int len = cArray.length;
		if (len < 2) return;

		for (int i=0; i<len; i++) {
			for (int j=i+1; j<len; j++) {
                if (cArray[i] == cArray[j]) {
                    cArray[i] = 0;
                    break;
                }
			}
		}

        System.out.println(String.valueOf(cArray));

		StringBuffer buffer = new StringBuffer();

		for (int i=0; i<len; i++) {
		    if (cArray[i] != 0) {
		        buffer.append(cArray[i]);
            }
        }

        System.out.println(buffer.toString());
    }

}
